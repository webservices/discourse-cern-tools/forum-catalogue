# Discourse Service Catalogue

Small application that contains all Discourse instances deployed at CERN

The location of the files are under `/eos/project/w/webservices/forum-catalogue`. Files are being served from webEOS infra, under the `forum-catalogue` project.
